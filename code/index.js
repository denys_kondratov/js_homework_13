// Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
// Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
// На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
// сторінок показувати нові коментарі. 
// з коментарів виводити : 
// "id": 1,
// "name"
// "email"
// "body":

const comments = document.getElementById("comments");

const url = "https://jsonplaceholder.typicode.com/comments";
const modalLoader = document.querySelector(".modal-loader");
const req = new XMLHttpRequest();

modalLoader.classList.add("active-loader");

req.open("GET", url);
req.send();
req.addEventListener("readystatechange", () => {   
   if(req.readyState === 4 && req.status >= 200 && req.status < 300){
      let arr = JSON.parse(req.responseText);
      //console.log(arr);

      Displaylist (arr, comments, row, curent_page);
      SetupPagination(arr, pagination_element, row);

      modalLoader.classList.remove("active-loader");
   }else if(req.readyState === 4){
      throw new Error("Помилка у запиті!");
   }
});

function showComments (data) {
   comments.append(createCommentCard(data))
};

function createCommentCard ({id, name, email, body, ...props}) {
   const div = document.createElement("div");
   div.insertAdjacentHTML("beforeend", `<div class="comment">
         <div class="comment__namber">${id}</div>
         <div class="comment__body">
            <div class="comment__user-info">
               <div class="user-name">${name}</div>
               <div class="user-email"> 
                  <a href="mailto:${email}" class="contact_mail">${email}</a>
               </div>
            </div>
            <div class="comment__line"></div>
            <div class="comment__text">${body}
            </div>
         </div>
      </div>
   `);   
   return div;
};

const pagination_element = document.getElementById("pagination");

let curent_page = 1;
let row = 10;

function Displaylist (items, wrapper, rows_per_page, page) {
   wrapper.innerHTML = "";
   page--;

   let start = rows_per_page * page;
   let end = start + rows_per_page;
   let paginatedItems = items.slice(start, end);
   //console.log(paginatedItems);

   for (let i = 0; i < paginatedItems.length; i++) {
      let item = paginatedItems[i];

      showComments(item);
   }
};

function SetupPagination (items, wrapper, rows_per_page) {
   wrapper.innerHTML = "";

   let page_count = Math.ceil(items.length / rows_per_page);
   for (let i = 1; i < page_count + 1; i++) {
      let btn =  PaginationButton(i, items);
      wrapper.appendChild(btn);
   }
};

function PaginationButton (page, items) {
   let button = document.createElement('button');
   button.innerText = page;

   if (curent_page == page) button.classList.add('active');

   button.addEventListener('click', function () {
      comments.innerText = "";
      curent_page = page;
      Displaylist(items, req, row, curent_page);

      let current_btn = document.querySelector('.pagination button.active');
      current_btn.classList.remove('active');

      button.classList.add('active');
   })
   return button;
}